package com.xiaolei.openokjoke

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDexApplication
import com.umeng.socialize.Config
import com.umeng.socialize.PlatformConfig
import com.umeng.socialize.UMShareAPI
import com.xiaolei.easyfreamwork.application.ApplicationBreage
import com.xiaolei.easyfreamwork.application.IApp
import com.xiaolei.openokjoke.Cache.FontCache
import com.xiaolei.openokjoke.JNI.JokeLib


/**
 * Created by xiaolei on 2017/12/6.
 */
class App : MultiDexApplication(), IApp
{
    /**
     * 友盟分享设置key
     */
    init
    {
        Config.DEBUG = BuildConfig.DEBUG
        PlatformConfig.setWeixin(JokeLib.getWXAPPID(), JokeLib.getWXAPPKEY())
        PlatformConfig.setQQZone(JokeLib.getQQAPPID(), JokeLib.getQQAPPKEY())
    }


    override fun onCreate()
    {
        val config = com.xiaolei.easyfreamwork.Config.Config()
        config.setDEBUG(BuildConfig.DEBUG)
        ApplicationBreage.getInstance().initApplication(this, config)
        registerActivityLifecycleCallbacks(LifeCycle)
        FontCache.initFont(this)
        UMShareAPI.get(this)
        super.onCreate()
    }
    override fun getApplication(): Application = this
    override fun getContext(): Context = this
}