package com.xiaolei.openokjoke.Net

import com.xiaolei.exretrofitcallback.network.regist.ResponseBeanRegister
import com.xiaolei.openokjoke.Base.ResBodyBean

/**
 * Created by xiaolei on 2017/7/21.
 */

class ResBodyRegister : ResponseBeanRegister<ResBodyBean<*>>()
{
    override fun filter(data: ResBodyBean<*>): String?
    {
        return data.callback
    }
}
